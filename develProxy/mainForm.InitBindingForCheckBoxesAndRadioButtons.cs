﻿using System.Windows.Forms;
using develProxy.Properties;

namespace develProxy
{
    public partial class MainForm
    {
        private void InitBindingForCheckBoxesAndRadioButtons()
        {
            Settings.Default.PropertyChanged += Settings_PropertyChanged;
            openBrowserAtStart.DataBindings.Add(
                new Binding("Checked", Settings.Default,
                    "OpenBrowserAtServerStart", true, DataSourceUpdateMode.OnPropertyChanged));
            showLogAuthorization.DataBindings.Add(new Binding("Checked", Settings.Default, "ShowLogAuthorization", true,
                DataSourceUpdateMode.OnPropertyChanged));
            showLogDebug.DataBindings.Add(new Binding("Checked", Settings.Default, "ShowLogDebug", true,
                DataSourceUpdateMode.OnPropertyChanged));
            showLogError.DataBindings.Add(new Binding("Checked", Settings.Default, "ShowLogError", true,
                DataSourceUpdateMode.OnPropertyChanged));
            showLogAbort.DataBindings.Add(new Binding("Checked", Settings.Default, "ShowLogAbort", true,
                DataSourceUpdateMode.OnPropertyChanged));
            settingsGroupBox.DataBindings.Add(new Binding("Enabled", startButton, "Enabled", true,
                DataSourceUpdateMode.OnPropertyChanged));

            normalLoginRadioButton.Checked = Settings.Default.Mode == Settings.NormalLoginMode;
            smallMarketRadioButton.Checked = Settings.Default.Mode == Settings.SmallMarketMode;
        }
    }
}