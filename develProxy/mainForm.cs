﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using develProxy.lib;
using develProxy.Properties;

namespace develProxy
{
    public partial class MainForm : Form
    {
        private readonly Logger _logger = new Logger();
        private ProxyServerBase _server;

        public MainForm()
        {
            InitializeComponent();
            InitBindingForCheckBoxesAndRadioButtons();
        }


        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Debug.WriteLine("Property changed: " + e.PropertyName);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            Validate();
            Settings.Default.Save();

            _logger.Clear();

            _server = ProxyServerFactory.CreateInstance(Settings.Default.GetProxySettings());
            _server.OnLog += ServerOnLog;
            _server.OnServerStart += () => { SetServerRunningDisplayMode(true); };
            _server.OnServerStop += () => { SetServerRunningDisplayMode(false); };

            _server.Start();
        }

        private void ServerOnLog(ServerLogItem item)
        {
            _logger.Add(item);

            BeginInvoke((MethodInvoker)(() =>
            {
                if (tabControl.SelectedIndex == 1)
                    RefreshLog();
            }));
        }

        private void RefreshLog()
        {
            BeginInvoke((MethodInvoker)(() =>
           {
               logRichTextBox.Text = _logger.GetTextForDisplay(GetCurrentLogItemTypes());
           }));

            IEnumerable<ServerLogItemType> GetCurrentLogItemTypes()
            {
                if (Settings.Default.ShowLogAbort) yield return ServerLogItemType.Abort;
                if (Settings.Default.ShowLogAuthorization) yield return ServerLogItemType.Authorization;
                if (Settings.Default.ShowLogDebug) yield return ServerLogItemType.Debug;
                if (Settings.Default.ShowLogError) yield return ServerLogItemType.Error;
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                SetServerRunningDisplayMode(false);
                if (_server == null) return;
                _server.Stop();
                _server.Dispose();
                GC.Collect();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void SetServerRunningDisplayMode(bool isRunning)
        {
            BeginInvoke((MethodInvoker)(() =>
           {
               stopButton.Enabled = isRunning;
               startButton.Enabled = !isRunning;
           }));

            if (isRunning) OpenBrowserIfNeeded();
        }

        private void OpenBrowserIfNeeded()
        {
            if (!Settings.Default.OpenBrowserAtServerStart) return;

            var url = _server.FirstLocalUrl;

            try
            {
                Process.Start(url);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"URL={url}\r\n{exception}", "Error when opening browser");
            }
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Validate();
            Settings.Default.Save();
        }

        private void RefreshLog(object sender, EventArgs e)
        {
            RefreshLog();
        }
       
        private void normalLoginRadioButton_Click(object sender, EventArgs e)
        {
            Settings.Default.Mode = Settings.NormalLoginMode;
        }

        private void smallMarketRadioButton_Click(object sender, EventArgs e)
        {
            Settings.Default.Mode = Settings.SmallMarketMode;
        }
    }
}