﻿namespace develProxy
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.mainPage = new System.Windows.Forms.TabPage();
            this.openBrowserAtStart = new System.Windows.Forms.CheckBox();
            this.stopButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.settingsGroupBox = new System.Windows.Forms.GroupBox();
            this.smallMarketRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RealSSNTextBox = new System.Windows.Forms.TextBox();
            this.remoteServerUrlForSmallMarketTextBox = new System.Windows.Forms.TextBox();
            this.CompanyOneCodeTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.normalLoginRadioButton = new System.Windows.Forms.RadioButton();
            this.normalLoginGroupBox = new System.Windows.Forms.GroupBox();
            this.remoteServerUrlForNormalLoginTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.localPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.localServerUrlTextBox = new System.Windows.Forms.TextBox();
            this.logPage = new System.Windows.Forms.TabPage();
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.logItemsSelectorGroupBox = new System.Windows.Forms.GroupBox();
            this.showLogAbort = new System.Windows.Forms.CheckBox();
            this.showLogError = new System.Windows.Forms.CheckBox();
            this.showLogDebug = new System.Windows.Forms.CheckBox();
            this.showLogAuthorization = new System.Windows.Forms.CheckBox();
            this.tabControl.SuspendLayout();
            this.mainPage.SuspendLayout();
            this.settingsGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.normalLoginGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.logPage.SuspendLayout();
            this.logItemsSelectorGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.mainPage);
            this.tabControl.Controls.Add(this.logPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(530, 490);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.RefreshLog);
            // 
            // mainPage
            // 
            this.mainPage.Controls.Add(this.openBrowserAtStart);
            this.mainPage.Controls.Add(this.stopButton);
            this.mainPage.Controls.Add(this.startButton);
            this.mainPage.Controls.Add(this.settingsGroupBox);
            this.mainPage.Location = new System.Drawing.Point(4, 22);
            this.mainPage.Name = "mainPage";
            this.mainPage.Padding = new System.Windows.Forms.Padding(3);
            this.mainPage.Size = new System.Drawing.Size(522, 464);
            this.mainPage.TabIndex = 0;
            this.mainPage.Text = "Main";
            this.mainPage.UseVisualStyleBackColor = true;
            // 
            // OpenBrowserAtServerStart
            // 
            this.openBrowserAtStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.openBrowserAtStart.AutoSize = true;
            this.openBrowserAtStart.Checked = global::develProxy.Properties.Settings.Default.OpenBrowserAtServerStart;
            this.openBrowserAtStart.Location = new System.Drawing.Point(192, 437);
            this.openBrowserAtStart.Name = "openBrowserAtStart";
            this.openBrowserAtStart.Size = new System.Drawing.Size(162, 17);
            this.openBrowserAtStart.TabIndex = 10;
            this.openBrowserAtStart.Text = "Open default browser at start";
            this.openBrowserAtStart.UseVisualStyleBackColor = true;
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(441, 433);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 12;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(360, 433);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 11;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // settingsGroupBox
            // 
            this.settingsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsGroupBox.Controls.Add(this.smallMarketRadioButton);
            this.settingsGroupBox.Controls.Add(this.groupBox1);
            this.settingsGroupBox.Controls.Add(this.normalLoginRadioButton);
            this.settingsGroupBox.Controls.Add(this.normalLoginGroupBox);
            this.settingsGroupBox.Controls.Add(this.groupBox2);
            this.settingsGroupBox.Controls.Add(this.label4);
            this.settingsGroupBox.Controls.Add(this.localPort);
            this.settingsGroupBox.Controls.Add(this.label1);
            this.settingsGroupBox.Controls.Add(this.localServerUrlTextBox);
            this.settingsGroupBox.Location = new System.Drawing.Point(3, 3);
            this.settingsGroupBox.Name = "settingsGroupBox";
            this.settingsGroupBox.Size = new System.Drawing.Size(511, 423);
            this.settingsGroupBox.TabIndex = 3;
            this.settingsGroupBox.TabStop = false;
            this.settingsGroupBox.Text = "Settings";
            // 
            // smallMarketRadioButton
            // 
            this.smallMarketRadioButton.AutoSize = true;
            this.smallMarketRadioButton.Location = new System.Drawing.Point(20, 126);
            this.smallMarketRadioButton.Name = "smallMarketRadioButton";
            this.smallMarketRadioButton.Size = new System.Drawing.Size(89, 17);
            this.smallMarketRadioButton.TabIndex = 5;
            this.smallMarketRadioButton.TabStop = true;
            this.smallMarketRadioButton.Text = "SM backdoor";
            this.smallMarketRadioButton.UseVisualStyleBackColor = true;
            this.smallMarketRadioButton.Click += new System.EventHandler(this.smallMarketRadioButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.RealSSNTextBox);
            this.groupBox1.Controls.Add(this.remoteServerUrlForSmallMarketTextBox);
            this.groupBox1.Controls.Add(this.CompanyOneCodeTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(6, 130);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(496, 75);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                                             ";
            // 
            // RealSSNTextBox
            // 
            this.RealSSNTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RealSSNTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "RealSSN", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RealSSNTextBox.Location = new System.Drawing.Point(358, 45);
            this.RealSSNTextBox.Name = "RealSSNTextBox";
            this.RealSSNTextBox.Size = new System.Drawing.Size(133, 20);
            this.RealSSNTextBox.TabIndex = 8;
            this.RealSSNTextBox.Text = global::develProxy.Properties.Settings.Default.RealSSN;
            // 
            // remoteServerUrlForSmallMarketTextBox
            // 
            this.remoteServerUrlForSmallMarketTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.remoteServerUrlForSmallMarketTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.remoteServerUrlForSmallMarketTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.remoteServerUrlForSmallMarketTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "RemoteUrl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.remoteServerUrlForSmallMarketTextBox.Location = new System.Drawing.Point(116, 22);
            this.remoteServerUrlForSmallMarketTextBox.Name = "remoteServerUrlForSmallMarketTextBox";
            this.remoteServerUrlForSmallMarketTextBox.Size = new System.Drawing.Size(375, 20);
            this.remoteServerUrlForSmallMarketTextBox.TabIndex = 6;
            this.remoteServerUrlForSmallMarketTextBox.Text = global::develProxy.Properties.Settings.Default.RemoteUrl;
            // 
            // CompanyOneCodeTextBox
            // 
            this.CompanyOneCodeTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CompanyOneCodeTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.CompanyOneCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "CompanyOneCode", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.CompanyOneCodeTextBox.Location = new System.Drawing.Point(115, 45);
            this.CompanyOneCodeTextBox.Name = "CompanyOneCodeTextBox";
            this.CompanyOneCodeTextBox.Size = new System.Drawing.Size(133, 20);
            this.CompanyOneCodeTextBox.TabIndex = 7;
            this.CompanyOneCodeTextBox.Text = global::develProxy.Properties.Settings.Default.CompanyOneCode;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Company One Code";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Remote Server URL";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(297, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Real SSN";
            // 
            // normalLoginRadioButton
            // 
            this.normalLoginRadioButton.AutoSize = true;
            this.normalLoginRadioButton.Location = new System.Drawing.Point(20, 67);
            this.normalLoginRadioButton.Name = "normalLoginRadioButton";
            this.normalLoginRadioButton.Size = new System.Drawing.Size(87, 17);
            this.normalLoginRadioButton.TabIndex = 3;
            this.normalLoginRadioButton.TabStop = true;
            this.normalLoginRadioButton.Text = "Normal Login";
            this.normalLoginRadioButton.UseVisualStyleBackColor = true;
            this.normalLoginRadioButton.Click += new System.EventHandler(this.normalLoginRadioButton_Click);
            // 
            // normalLoginGroupBox
            // 
            this.normalLoginGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.normalLoginGroupBox.Controls.Add(this.remoteServerUrlForNormalLoginTextBox);
            this.normalLoginGroupBox.Controls.Add(this.label5);
            this.normalLoginGroupBox.Location = new System.Drawing.Point(6, 71);
            this.normalLoginGroupBox.Name = "normalLoginGroupBox";
            this.normalLoginGroupBox.Size = new System.Drawing.Size(496, 49);
            this.normalLoginGroupBox.TabIndex = 4;
            this.normalLoginGroupBox.TabStop = false;
            this.normalLoginGroupBox.Text = "                          ";
            // 
            // remoteServerUrlForNormalLoginTextBox
            // 
            this.remoteServerUrlForNormalLoginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.remoteServerUrlForNormalLoginTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.remoteServerUrlForNormalLoginTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.remoteServerUrlForNormalLoginTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "MasLoginUrl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.remoteServerUrlForNormalLoginTextBox.Location = new System.Drawing.Point(115, 19);
            this.remoteServerUrlForNormalLoginTextBox.Name = "remoteServerUrlForNormalLoginTextBox";
            this.remoteServerUrlForNormalLoginTextBox.Size = new System.Drawing.Size(375, 20);
            this.remoteServerUrlForNormalLoginTextBox.TabIndex = 4;
            this.remoteServerUrlForNormalLoginTextBox.Text = global::develProxy.Properties.Settings.Default.MasLoginUrl;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "MAS Login URL";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(6, 211);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(496, 205);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Regexes to define a remote server request";
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "DataRequestRegexs", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(490, 186);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = global::develProxy.Properties.Settings.Default.DataRequestRegexs;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.CausesValidation = false;
            this.label4.Location = new System.Drawing.Point(32, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Local Proxy port";
            // 
            // LocalPort
            // 
            this.localPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "LocalPort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.localPort.Location = new System.Drawing.Point(121, 19);
            this.localPort.Name = "localPort";
            this.localPort.Size = new System.Drawing.Size(51, 20);
            this.localPort.TabIndex = 1;
            this.localPort.Text = global::develProxy.Properties.Settings.Default.LocalPort;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Local Server URL";
            // 
            // localServerUrlTextBox
            // 
            this.localServerUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.localServerUrlTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::develProxy.Properties.Settings.Default, "LocalUrl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.localServerUrlTextBox.Location = new System.Drawing.Point(121, 45);
            this.localServerUrlTextBox.Name = "localServerUrlTextBox";
            this.localServerUrlTextBox.Size = new System.Drawing.Size(381, 20);
            this.localServerUrlTextBox.TabIndex = 2;
            this.localServerUrlTextBox.Text = global::develProxy.Properties.Settings.Default.LocalUrl;
            // 
            // logPage
            // 
            this.logPage.Controls.Add(this.logRichTextBox);
            this.logPage.Controls.Add(this.logItemsSelectorGroupBox);
            this.logPage.Location = new System.Drawing.Point(4, 22);
            this.logPage.Name = "logPage";
            this.logPage.Size = new System.Drawing.Size(522, 464);
            this.logPage.TabIndex = 1;
            this.logPage.Text = "Log";
            this.logPage.UseVisualStyleBackColor = true;
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logRichTextBox.Location = new System.Drawing.Point(8, 53);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.ReadOnly = true;
            this.logRichTextBox.Size = new System.Drawing.Size(505, 376);
            this.logRichTextBox.TabIndex = 1;
            this.logRichTextBox.Text = "";
            this.logRichTextBox.WordWrap = false;
            // 
            // logItemsSelectorGroupBox
            // 
            this.logItemsSelectorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logItemsSelectorGroupBox.Controls.Add(this.showLogAbort);
            this.logItemsSelectorGroupBox.Controls.Add(this.showLogError);
            this.logItemsSelectorGroupBox.Controls.Add(this.showLogDebug);
            this.logItemsSelectorGroupBox.Controls.Add(this.showLogAuthorization);
            this.logItemsSelectorGroupBox.Location = new System.Drawing.Point(8, 3);
            this.logItemsSelectorGroupBox.Name = "logItemsSelectorGroupBox";
            this.logItemsSelectorGroupBox.Size = new System.Drawing.Size(505, 44);
            this.logItemsSelectorGroupBox.TabIndex = 0;
            this.logItemsSelectorGroupBox.TabStop = false;
            this.logItemsSelectorGroupBox.Text = "Log items to display";
            // 
            // ShowLogAbort
            // 
            this.showLogAbort.AutoSize = true;
            this.showLogAbort.Location = new System.Drawing.Point(427, 19);
            this.showLogAbort.Name = "showLogAbort";
            this.showLogAbort.Size = new System.Drawing.Size(67, 17);
            this.showLogAbort.TabIndex = 5;
            this.showLogAbort.Text = "Blockers";
            this.showLogAbort.UseVisualStyleBackColor = true;
            this.showLogAbort.Click += new System.EventHandler(this.RefreshLog);
            // 
            // ShowLogError
            // 
            this.showLogError.AutoSize = true;
            this.showLogError.Location = new System.Drawing.Point(371, 19);
            this.showLogError.Name = "showLogError";
            this.showLogError.Size = new System.Drawing.Size(53, 17);
            this.showLogError.TabIndex = 4;
            this.showLogError.Text = "Errors";
            this.showLogError.UseVisualStyleBackColor = true;
            this.showLogError.Click += new System.EventHandler(this.RefreshLog);
            // 
            // ShowLogDebug
            // 
            this.showLogDebug.AutoSize = true;
            this.showLogDebug.Location = new System.Drawing.Point(307, 19);
            this.showLogDebug.Name = "showLogDebug";
            this.showLogDebug.Size = new System.Drawing.Size(58, 17);
            this.showLogDebug.TabIndex = 3;
            this.showLogDebug.Text = "Debug";
            this.showLogDebug.UseVisualStyleBackColor = true;
            this.showLogDebug.Click += new System.EventHandler(this.RefreshLog);
            // 
            // ShowLogAuthorization
            // 
            this.showLogAuthorization.AutoSize = true;
            this.showLogAuthorization.Location = new System.Drawing.Point(214, 19);
            this.showLogAuthorization.Name = "showLogAuthorization";
            this.showLogAuthorization.Size = new System.Drawing.Size(87, 17);
            this.showLogAuthorization.TabIndex = 2;
            this.showLogAuthorization.Text = "Authorization";
            this.showLogAuthorization.UseVisualStyleBackColor = true;
            this.showLogAuthorization.Click += new System.EventHandler(this.RefreshLog);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 490);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(540, 418);
            this.Name = "MainForm";
            this.Text = "MBC UI Proxy Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.mainPage.ResumeLayout(false);
            this.mainPage.PerformLayout();
            this.settingsGroupBox.ResumeLayout(false);
            this.settingsGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.normalLoginGroupBox.ResumeLayout(false);
            this.normalLoginGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.logPage.ResumeLayout(false);
            this.logItemsSelectorGroupBox.ResumeLayout(false);
            this.logItemsSelectorGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage mainPage;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.GroupBox settingsGroupBox;
        private System.Windows.Forms.TextBox remoteServerUrlForNormalLoginTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox localServerUrlTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox localPort;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox openBrowserAtStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox normalLoginGroupBox;
        private System.Windows.Forms.RadioButton normalLoginRadioButton;
        private System.Windows.Forms.TabPage logPage;
        private System.Windows.Forms.GroupBox logItemsSelectorGroupBox;
        private System.Windows.Forms.CheckBox showLogAuthorization;
        private System.Windows.Forms.CheckBox showLogAbort;
        private System.Windows.Forms.CheckBox showLogError;
        private System.Windows.Forms.CheckBox showLogDebug;
        private System.Windows.Forms.RichTextBox logRichTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox RealSSNTextBox;
        private System.Windows.Forms.TextBox remoteServerUrlForSmallMarketTextBox;
        private System.Windows.Forms.TextBox CompanyOneCodeTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton smallMarketRadioButton;
    }
}

