﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using develProxy.lib;

namespace develProxy
{
    public class Logger
    {
        private readonly object _locker = new object();
        private readonly List<ServerLogItem> _messages = new List<ServerLogItem>();

        public void Add(ServerLogItem message)
        {
            lock (_locker)
            {
                _messages.Add(message);
            }
        }

        public string GetTextForDisplay(IEnumerable<ServerLogItemType> types)
        {
            var result = new StringBuilder();
            lock (_locker)
            {
                foreach (var message in _messages.Where(a => types.Contains(a.Type)))
                    result.Append(message.Message + "\r\n");
            }

            return result.ToString();
        }

        public void Clear()
        {
            lock (_locker)
            {
                _messages.Clear();
            }
        }
    }
}