﻿using develProxy.lib;

namespace develProxy.Properties
{
    public sealed partial class Settings
    {
        public const string NormalLoginMode = "normalLogin";
        public const string SmallMarketMode = "smallMarket";

        public ProxySettings GetProxySettings()
        {
            var proxySettings = new ProxySettings
            {
                LocalUrl = LocalUrl,
                ProxyHost = "localhost",
                ProxyPort = LocalPort,
                DataRequestRegexs = DataRequestRegexs
            };

            switch (Mode)
            {
                case NormalLoginMode:
                    proxySettings.SpecificSettings = new MasLoginSettings();
                    proxySettings.RemoteUrl = MasLoginUrl;
                    break;
                case SmallMarketMode:
                    proxySettings.SpecificSettings = new SmallMarketSettings
                    {
                        RealSsn = RealSSN,
                        CompanyOneCode = CompanyOneCode
                    };
                    proxySettings.RemoteUrl = RemoteUrl;
                    break;
            }

            return proxySettings;
        }
    }
}