﻿namespace develProxy.lib
{
    public class ServerLogItem
    {
        public string Message { get; set; }

        public ServerLogItemType Type { get; set; }
    }
}