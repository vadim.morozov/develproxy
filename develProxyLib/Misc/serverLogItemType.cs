﻿namespace develProxy.lib
{
    public enum ServerLogItemType
    {
        Authorization,
        Error,
        Debug,
        Abort
    }
}