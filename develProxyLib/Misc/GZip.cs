using System.IO;
using System.IO.Compression;

namespace develProxy.lib
{
    public static class GZip
    {
        public static byte[] Decompress(byte[] source)
        {
            using var stream = new GZipStream(new MemoryStream(source),
                CompressionMode.Decompress);
            using var memory = new MemoryStream();
            var buffer = new byte[4096];
            int count;
            while ((count = stream.Read(buffer, 0, buffer.Length)) > 0) memory.Write(buffer, 0, count);
            memory.Close();
            return memory.ToArray();
        }

        public static byte[] Compress(byte[] source)
        {
            using var baseStream = new MemoryStream();
            using var stream = new GZipStream(baseStream,
                CompressionMode.Compress);
            stream.Write(source, 0, source.Length);
            stream.Close();
            baseStream.Close();
            return baseStream.ToArray();
        }
    }
}