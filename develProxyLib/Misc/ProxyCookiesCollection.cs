﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace develProxy.lib
{
    public class ProxyCookiesCollection
    {
        private readonly List<Cookie> _cookies = new List<Cookie>();

        private readonly object _locker = new object();

        public int Count => _cookies.Count;

        public IEnumerable<Cookie> GetCookies(Uri uri)
        {
            return _cookies.Select(cookie => cookie.Clone(uri)).ToArray();
        }

        public void AddOrReplace(IEnumerable<Cookie> newCookies)
        {
            lock (_locker)
            {
                foreach (var cookie in _cookies.ToArray()
                    .Where(existingCookie =>
                        newCookies.Select(newCookie => newCookie.Name)
                            .Contains(existingCookie.Name)))
                    _cookies.Remove(cookie);

                _cookies.AddRange(newCookies);
            }
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            _cookies.ForEach(cookie => str.Append($"\r\n{cookie.Name}: {cookie.Value}"));
            return str.ToString();
        }
    }
}