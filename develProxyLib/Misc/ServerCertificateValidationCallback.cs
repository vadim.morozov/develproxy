﻿using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace develProxy.lib
{
    public static class Extensions
    {
        public static bool ServerCertificateValidationCallback(object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslpolicyerrors)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls |
                                                   SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            return true;
        }
    }
}