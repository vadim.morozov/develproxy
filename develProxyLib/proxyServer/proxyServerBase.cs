﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace develProxy.lib
{
    public abstract partial class ProxyServerBase : IDisposable
    {
        public delegate void OnServerStartDelegate();
        public delegate void OnServerStopDelegate();
        public event OnServerStartDelegate OnServerStart;
        public event OnServerStopDelegate OnServerStop;
        /// <summary>
        /// Url that should be opened in browser at start
        /// </summary>
        public abstract string FirstLocalUrl { get; }
        protected ProxySettings Settings { get; }
        private const string DefaultUserAgent =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36";

        private readonly ConcurrentBag<Task> _allRequestTasks = new ConcurrentBag<Task>();
        private readonly Regex[] _dataPathRegexList;
        private HttpListener _listener = new HttpListener();
        private Task _listenerTask;

        private protected ProxyServerBase(ProxySettings settings)
        {
            Settings = settings;
            _dataPathRegexList = settings.DataRequestRegexs
                .Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(a => new Regex(a, RegexOptions.IgnoreCase))
                .ToArray();
        }

        public static HttpWebRequest CreateGetRequest(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.AllowAutoRedirect = false;
            request.UserAgent = DefaultUserAgent;
            request.CookieContainer = new CookieContainer();
            return request;
        }

        public void Start()
        {
            if (_listener.IsListening) throw new Exception("Server already started");
            _listenerTask = Task.Factory.StartNew(ListeningProcess);
        }

        public void Stop()
        {
            _listener?.Stop();
        }

        public void Dispose()
        {
            try
            {
                Task.WaitAll(_allRequestTasks.ToArray());
            }
            finally
            {
                foreach (var task in _allRequestTasks) task.Dispose();
            }
            _listenerTask.Dispose();
        }

        protected abstract bool AuthorizationProcess();
        protected abstract void ProcessRequest(HttpListenerContext context);

        protected bool IsDataRequest(string absolutePath)
        {
            return _dataPathRegexList.Any(a => a.IsMatch(absolutePath));
        }

        private async void ListeningProcess()
        {
            OnServerStart?.Invoke();
            var backdoorTask = Task.Factory.StartNew(AuthorizationProcess);

            _listener = GetAndStartListenerOrNull();
            var tokenSource = new CancellationTokenSource();

            while (_listener != null && _listener.IsListening)
                try
                {
                    var context = await _listener.GetContextAsync();
                    var backdoorTaskSucceeded = await backdoorTask;
                    if (!backdoorTaskSucceeded)
                    {
                        Stop();
                        break;
                    }

                    _allRequestTasks.Add(
                        Task.Factory.StartNew(
                            () => { ProcessRequest(context); },
                            tokenSource.Token));
                }
                catch (Exception e)
                {
                    Log(ServerLogItemType.Error, e.ToString());
                }

            tokenSource.Cancel();
            OnServerStop?.Invoke();
        }

        private HttpListener GetAndStartListenerOrNull()
        {
            try
            {
                var listener = new HttpListener();
                listener.Prefixes.Add($"http://{Settings.ProxyHost}:{Settings.ProxyPort}/");
                listener.Start();
                return listener;
            }
            catch (Exception ex)
            {
                Log(ServerLogItemType.Abort, @"Error while starting server\r\n" + ex);
            }

            return null;
        }
    }
}