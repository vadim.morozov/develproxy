﻿using System.Diagnostics;

namespace develProxy.lib
{
    public abstract partial class ProxyServerBase
    {
        public delegate void OnLogDelegate(ServerLogItem item);

        public event OnLogDelegate OnLog;

        internal void Log(ServerLogItemType type, string message)
        {
            OnLog?.Invoke(new ServerLogItem {Type = type, Message = message});
            Debug.WriteLine(type + " " + message);

            if (type == ServerLogItemType.Abort) 
                Stop();
        }
    }
}