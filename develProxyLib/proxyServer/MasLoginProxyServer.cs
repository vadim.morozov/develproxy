﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace develProxy.lib
{
    public class MasLoginProxyServer : ProxyServerBase
    {
        private readonly string _baseMasUrl;
        private readonly MasLoginPageRequestProcessor _masLoginPageRequestProcessor;
        private RequestProcessor _localRequestProcessor;
        private RequestProcessor _remoteRequestProcessor;
        private Task<bool> _signMeInTask;

        public MasLoginProxyServer(ProxySettings settings) : base(settings)
        {
            _baseMasUrl = new Uri(settings.RemoteUrl).GetBaseUrl();
            _masLoginPageRequestProcessor =
                new MasLoginPageRequestProcessor(Log, _baseMasUrl, $"http://localhost:{settings.ProxyPort}");
        }

        public override string FirstLocalUrl
        {
            get
            {
                var masUri = new Uri(Settings.RemoteUrl);
                return $"http://localhost:{Settings.ProxyPort}{masUri.PathAndQuery}";
            }
        }

        protected override async void ProcessRequest(HttpListenerContext context)
        {
            if (context.Request.Url.AbsolutePath == "/signmein")
            {
                _signMeInTask = new TaskFactory().StartNew(() => ProcessSignMeInRequest(context));
                return;
            }

            if (_remoteRequestProcessor == null)
            {
                _masLoginPageRequestProcessor.ProcessRequest(context);
                return;
            }

            var signMeInSuccess = await _signMeInTask;

            if (!signMeInSuccess)
            {
                Log(ServerLogItemType.Abort, "SignMeIn request returns no required cookies or can't get security token from portal page");
                return;
            }

            if (IsDataRequest(context.Request.Url.AbsolutePath))
                _remoteRequestProcessor.ProcessRequest(context);
            else
                _localRequestProcessor.ProcessRequest(context);
        }

        private bool ProcessSignMeInRequest(HttpListenerContext context)
        {
            var portalUrl = _masLoginPageRequestProcessor.PortalUrl;
            var signMeInRequestProcessor = new SignMeInRequestProcessor(Log, portalUrl, _baseMasUrl);
            signMeInRequestProcessor.ProcessRequest(context);
            _localRequestProcessor = new RequestProcessor(Log, Settings.LocalUrl);
            _remoteRequestProcessor =
                new RequestProcessor(Log, _masLoginPageRequestProcessor.PortalUrl, signMeInRequestProcessor.Cookies);
            return _remoteRequestProcessor.SetSecurityTokenAndCookieFromFirstPage();
        }

        protected override bool AuthorizationProcess()
        {
            return true;
        }
    }
}