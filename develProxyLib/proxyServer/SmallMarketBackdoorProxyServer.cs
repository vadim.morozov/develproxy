﻿using System.Linq;
using System.Net;
using System.Text;

namespace develProxy.lib
{
    public class SmallMarketBackdoorProxyServer : ProxyServerBase
    {
        private readonly RequestProcessor _localRequestProcessor;
        private readonly RequestProcessor _remoteRequestProcessor;

        public SmallMarketBackdoorProxyServer(ProxySettings settings, string remoteUrl) : base(settings)
        {
            _localRequestProcessor = new RequestProcessor(Log, settings.LocalUrl);
            _remoteRequestProcessor = new RequestProcessor(Log, remoteUrl);
        }

        private SmallMarketSettings SpecificSettings => Settings.SpecificSettings as SmallMarketSettings;

        public override string FirstLocalUrl => $"http://localhost:{Settings.ProxyPort}";

        protected override bool AuthorizationProcess()
        {
            var result = BackdoorRequest() && SignMeInRequest() &&
                         _remoteRequestProcessor.SetSecurityTokenAndCookieFromFirstPage();

            if (result)
                Log(ServerLogItemType.Authorization, "Backdoor process successfully finished");
            else
                Log(ServerLogItemType.Abort, "Backdoor process failed");

            return result;

            bool BackdoorRequest()
            {
                var url =
                    $"{Settings.RemoteUrl}/SSO/Authenticate/DEVSSO?userid={SpecificSettings.RealSsn}&company={SpecificSettings.CompanyOneCode}";
                var request = CreateGetRequest(url);
                using var response = (HttpWebResponse) request.GetResponse();
                _remoteRequestProcessor.Cookies.AddOrReplace(response.Cookies.AsEnumerable());
                if (_remoteRequestProcessor.Cookies.Count >= 5 && response.StatusCode == HttpStatusCode.Found)
                {
                    Log(ServerLogItemType.Authorization, $"Correct response from {url}");
                    return true;
                }

                var logStringBuilder = new StringBuilder();
                logStringBuilder
                    .Append($"{url} returns no enough cookies or invalid http status code.")
                    .Append("\r\nMust be at least 5 cookies\r\nPresented:");
                _remoteRequestProcessor.Cookies.GetCookies(request.RequestUri)
                    .ToList()
                    .ForEach(cookie => logStringBuilder.Append($"\r\n{cookie.Name}: {cookie.Value}"));
                logStringBuilder
                    .Append(
                        $"\r\nHttp status code must be 302 (Found), Presented: {response.StatusCode} ({(int) response.StatusCode})")
                    .Append($"\r\nResponse data:\r\n{response.GetResponseText()}");

                Log(ServerLogItemType.Error, logStringBuilder.ToString());

                return false;
            }

            bool SignMeInRequest()
            {
                var url = $"{Settings.RemoteUrl}/signMeIn";

                var request = CreateGetRequest(url);
                request.FillCookiesFrom(_remoteRequestProcessor.Cookies);

                using var response = (HttpWebResponse) request.GetResponse();
                var newCookies = response.Cookies.AsEnumerable();
                if (response.StatusCode == HttpStatusCode.Found)
                {
                    _remoteRequestProcessor.Cookies.AddOrReplace(newCookies);
                    Log(ServerLogItemType.Authorization, $"Correct response from {url}");
                    return true;
                }

                var logStringBuilder = new StringBuilder();
                logStringBuilder
                    .Append($"{url} returns no enough cookies or invalid http status code.")
                    .Append("\r\nMust be at least 2 cookies\r\nPresented:");
                newCookies.ToList().ForEach(cookie => logStringBuilder.Append($"\r\n{cookie.Name}: {cookie.Value}"));
                logStringBuilder
                    .Append(
                        $"\r\nHttp status code must be 302 (Found), Presented: {response.StatusCode} ({(int) response.StatusCode})")
                    .Append($"\r\nResponse data:\r\n{response.GetResponseText()}");

                return false;
            }
        }

        protected override void ProcessRequest(HttpListenerContext context)
        {
            if (IsDataRequest(context.Request.Url.AbsolutePath))
                _remoteRequestProcessor.ProcessRequest(context);
            else
                _localRequestProcessor.ProcessRequest(context);
        }
    }
}