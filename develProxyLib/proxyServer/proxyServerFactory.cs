﻿using System;

namespace develProxy.lib
{
    public static class ProxyServerFactory
    {
        public static ProxyServerBase CreateInstance(ProxySettings settings)
        {
            switch (settings.SpecificSettings)
            {
                case SmallMarketSettings smallMarketSettings:
                    return new SmallMarketBackdoorProxyServer(settings, settings.RemoteUrl);
                case MasLoginSettings loginSettings:
                    return new MasLoginProxyServer(settings);
            }

            throw new ArgumentException("Invalid settings.specificSettings type in proxyServerBase.createProxyServer");
        }
    }
}