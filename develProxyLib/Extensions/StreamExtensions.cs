﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace develProxy.lib
{
    public static class StreamExtensions
    {
        public static byte[] ReadAllBytes(this Stream stream)
        {
            if (stream == null || !stream.CanRead) return new byte[] { };

            var buffer = new byte[1024];
            int len;
            var streamContent = new List<byte>();
            while ((len = stream.Read(buffer, 0, 1024)) > 0) streamContent.AddRange(buffer.Take(len));

            return streamContent.ToArray();
        }
    }
}