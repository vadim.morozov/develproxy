﻿using System.Net;
using System.Text;

namespace develProxy.lib
{
    public static class HttpWebResponseExtensions
    {
        public static string GetResponseText(this HttpWebResponse response)
        {
            using var stream = response.GetResponseStream();

            var streamContentArray = stream.ReadAllBytes();

            var answerText = Encoding.UTF8.GetString(
                response.ContentEncoding == "gzip"
                    ? GZip.Decompress(streamContentArray)
                    : streamContentArray);

            return answerText;
        }
    }
}