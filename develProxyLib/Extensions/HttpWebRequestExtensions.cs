﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace develProxy.lib
{
    public static class HttpWebRequestExtensions
    {
        public static void FillCookiesFrom(this HttpWebRequest request,
            ProxyCookiesCollection required, CookieCollection optionalNullAllowed = null)
        {
            var allCookies = new List<Cookie>();
            allCookies.AddRange(required.GetCookies(request.RequestUri));
            if (optionalNullAllowed != null)
                allCookies.AddRange(
                    optionalNullAllowed.AsEnumerable()
                        .Where(a => allCookies.All(b => b.Name != a.Name))
                        .Select(a => a.Clone(request.RequestUri))
                );

            foreach (var cookie in allCookies) request.CookieContainer.Add(cookie);
        }
    }
}