﻿using System;
using System.Text;

namespace develProxy.lib
{
    public static class UriExtensions
    {
        public static string GetBaseUrl(this Uri uri)
        {
            var str = new StringBuilder();
            str.Append(uri.Scheme).Append("://").Append(uri.Host);
            if (uri.Scheme == "http" && uri.Port != 80 || uri.Scheme == "https" && uri.Port != 443)
                str.Append(":").Append(uri.Port);

            return str.ToString();
        }
    }
}