﻿using System;
using System.Collections.Generic;
using System.Net;

namespace develProxy.lib
{
    public static class CookiesExtensions
    {
        public static Cookie Clone(this Cookie source, Uri uri = null)
        {
            return new Cookie
            {
                Name = source.Name,
                Value = source.Value,
                Domain = uri.Host,
                Path = source.Path,
                HttpOnly = source.HttpOnly,
                Secure = source.Secure,
                Expired = source.Expired,
                Expires = source.Expires
            };
        }

        public static IEnumerable<Cookie> AsEnumerable(this CookieCollection cookies)
        {
            foreach (var cookieObject in cookies)
                if (cookieObject is Cookie cookie)
                    yield return cookie;
        }
    }
}