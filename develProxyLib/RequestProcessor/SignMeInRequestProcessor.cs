﻿using System.Collections.Generic;
using System.Net;

namespace develProxy.lib
{
    public class SignMeInRequestProcessor : RequestProcessor
    {
        private readonly string _originUrl;

        public SignMeInRequestProcessor(LogDelegate log,
            string baseRemoteUrl,
            string originUrl
        ) : base(log, baseRemoteUrl)
        {
            _originUrl = originUrl;
        }

        protected override Dictionary<string, string> GetHeadersFromContextRequest(HttpListenerRequest contextRequest,
            HttpWebRequest request)
        {
            var result = base.GetHeadersFromContextRequest(contextRequest, request);

            result["Origin"] = _originUrl;
            result["Referer"] = _originUrl + "/";
            result["Sec-Fetch-Site"] = "cross-site";

            return result;
        }
    }
}