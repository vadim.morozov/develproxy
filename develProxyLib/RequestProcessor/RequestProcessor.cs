﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace develProxy.lib
{
    public class RequestProcessor
    {
        public delegate void LogDelegate(ServerLogItemType type, string message);

        private const string SecurityTokenHeaderName = "X-XSRF-Token";

        private readonly string _baseUrl;

        protected readonly LogDelegate Log;

        public RequestProcessor(LogDelegate log, string baseRemoteUrl, ProxyCookiesCollection cookies = null,
            string securityTokenValue = null)
        {
            Log = log;
            _baseUrl = baseRemoteUrl;
            Cookies = cookies ?? new ProxyCookiesCollection();
            SecurityTokenValue = securityTokenValue;
        }

        public ProxyCookiesCollection Cookies { get; }
        private string SecurityTokenValue { get; set; }

        public void ProcessRequest(HttpListenerContext context)
        {
            HttpWebRequest remoteRequest = null;
            HttpWebResponse remoteResponse = null;

            var result = CreateRequest()
                         && GetResponse()
                         && CreateResponse();

            if (result && remoteRequest != null && remoteResponse != null)
                Log(ServerLogItemType.Debug,
                    $"{remoteRequest.RequestUri} {remoteResponse.StatusDescription} ({(int) remoteResponse.StatusCode})");

            bool CreateRequest()
            {
                try
                {
                    remoteRequest = CreateRemoteRequestFromContextRequest(context);
                }
                catch (Exception createRequestException)
                {
                    Log(ServerLogItemType.Error,
                        $"{context.Request.Url} CreateRemoteRequestFromContextRequest returns exception:\r\n{createRequestException}");
                    return false;
                }

                return true;
            }

            bool GetResponse()
            {
                try
                {
                    remoteResponse = (HttpWebResponse) remoteRequest.GetResponse();
                    return true;
                }
                catch (WebException webException)
                {
                    if (webException.Response == null)
                    {
                        Log(ServerLogItemType.Debug,
                            $"{remoteRequest.RequestUri} returns unknown WebException:\r\n{webException}");
                        return false;
                    }

                    remoteResponse = (HttpWebResponse) webException.Response;
                    return true;
                }
                catch (Exception getResponseException)
                {
                    Log(ServerLogItemType.Error,
                        $"{context.Request.Url} CreateRemoteRequestFromContextRequest returns exception:\r\n{getResponseException}");
                    return false;
                }
            }

            bool CreateResponse()
            {
                try
                {
                    Cookies.AddOrReplace(remoteResponse.Cookies.AsEnumerable());
                    FillBrowserResponseFromRemoteResponse(context.Response, remoteResponse);
                    remoteResponse.Close();
                    return true;
                }
                catch (Exception createResponseException)
                {
                    Log(ServerLogItemType.Error,
                        $"{context.Request.Url} CreateRemoteRequestFromContextRequest returns exception:\r\n{createResponseException}");
                    return false;
                }
            }
        }

        #region CreateRemoteRequestFromContextRequest

        private HttpWebRequest CreateRemoteRequestFromContextRequest(HttpListenerContext context)
        {
            var contextRequest = context.Request;
            var request = (HttpWebRequest) WebRequest.Create(_baseUrl + context.Request.Url.PathAndQuery);

            SetRemoteRequestFields(contextRequest, request);
            SetRemoteRequestHeaders(contextRequest, request);
            SetRemoteRequestCookies(request);
            SetRemoteRequestData(contextRequest, request);

            return request;
        }

        private void SetRemoteRequestFields(HttpListenerRequest contextRequest, HttpWebRequest request)
        {
            request.Method = contextRequest.HttpMethod;
            request.AllowAutoRedirect = false;
            request.UserAgent = contextRequest.UserAgent;
            request.ServerCertificateValidationCallback = Extensions.ServerCertificateValidationCallback;
            request.ContentType = contextRequest.ContentType;
            request.Accept = contextRequest.AcceptTypes?.Aggregate("",
                (str, item) => str + (string.IsNullOrWhiteSpace(str) ? ", " : "") + item);
        }

        private void SetRemoteRequestCookies(HttpWebRequest request)
        {
            request.CookieContainer = new CookieContainer();
            request.FillCookiesFrom(Cookies);
        }

        protected virtual Dictionary<string, string> GetHeadersFromContextRequest(HttpListenerRequest contextRequest,
            HttpWebRequest request)
        {
            var result = new Dictionary<string, string>();
            var headers = contextRequest.Headers;
            var requestHostAndPort = request.RequestUri.GetBaseUrl();
            var originalHostAndPort = contextRequest.Url.GetBaseUrl();

            foreach (string key in headers.Keys)
            {
                var originalValue = headers[key];
                string modifiedValue;
                switch (key)
                {
                    case "Origin":
                    case "Referer":
                        modifiedValue = originalValue?.Replace(originalHostAndPort, requestHostAndPort);
                        break;
                    default:
                        modifiedValue = originalValue;
                        break;
                }

                result.Add(key, modifiedValue);
            }

            if (!string.IsNullOrWhiteSpace(SecurityTokenValue)) result[SecurityTokenHeaderName] = SecurityTokenValue;

            return result;
        }

        private void SetRemoteRequestHeaders(HttpListenerRequest contextRequest, HttpWebRequest request)
        {
            var modifiedHeaders = GetHeadersFromContextRequest(contextRequest, request);

            foreach (var key in modifiedHeaders.Keys)
            {
                var value = modifiedHeaders[key];
                switch (key)
                {
                    case "Connection":
                    case "Content-Type":
                    case "Content-Length":
                    case "Host":
                    case "User-Agent":
                    case SecurityTokenHeaderName:
                        break;
                    case "Accept":
                        request.Accept = value;
                        break;
                    case "Referer":
                        request.Referer = value;
                        break;
                    case "If-Modified-Since":
                        if (DateTime.TryParse(value, out var d))
                            request.IfModifiedSince = d;
                        break;
                    default:
                        request.Headers.Add(key, value);
                        break;
                }
            }

            SetSecurityHeaderIfNeeded();

            void SetSecurityHeaderIfNeeded()
            {
                if (string.IsNullOrWhiteSpace(SecurityTokenValue)) return;

                var securityHeader = request.Headers[SecurityTokenHeaderName];
                if (securityHeader != null) request.Headers.Remove(securityHeader);

                request.Headers.Add(SecurityTokenHeaderName, SecurityTokenValue);
            }
        }

        private void SetRemoteRequestData(HttpListenerRequest contextRequest, HttpWebRequest request)
        {
            var data = GetDataFromBrowserRequest(contextRequest);
            var modifiedData = ModifyBrowserRequestData(data);

            SetDataToRemoteRequest(request, modifiedData);
        }

        private byte[] ModifyBrowserRequestData(byte[] data)
        {
            return data;
        }

        private void SetDataToRemoteRequest(HttpWebRequest request, byte[] data)
        {
            var dataLength = data.Length;

            if (dataLength == 0) return;

            request.ContentLength = dataLength;

            var requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();
        }

        private byte[] GetDataFromBrowserRequest(HttpListenerRequest contextRequest)
        {
            if (contextRequest.HttpMethod == "GET") return new byte[] { };
            return contextRequest.InputStream.ReadAllBytes();
        }

        #endregion

        #region FillBrowserResponseFromRemoteResponse

        private void FillBrowserResponseFromRemoteResponse(HttpListenerResponse browserResponse,
            HttpWebResponse response)
        {
            SetBrowserResponseFields(browserResponse, response);
            SetBrowserResponseCookies(browserResponse, response);
            SetBrowserResponseHeaders(browserResponse, response);
            SetBrowserResponseData(browserResponse, response);
        }

        private void SetBrowserResponseFields(HttpListenerResponse browserResponse, HttpWebResponse response)
        {
            browserResponse.ContentType = response.ContentType;
            browserResponse.SendChunked = response.ContentType == "chunked";
            browserResponse.StatusCode = (int) response.StatusCode;
            browserResponse.StatusDescription = response.StatusDescription;
        }

        private void SetBrowserResponseCookies(HttpListenerResponse browserResponse, HttpWebResponse response)
        {
            foreach (var cookie in response.Cookies.AsEnumerable()) browserResponse.SetCookie(cookie);
        }

        private void SetBrowserResponseHeaders(HttpListenerResponse browserResponse, HttpWebResponse response)
        {
            var headers = response.Headers;
            foreach (string key in response.Headers.Keys)
                try
                {
                    var value = headers[key];

                    switch (key)
                    {
                        case "Content-Length":
                            break;
                        case "Keep-Alive":
                            browserResponse.KeepAlive = true;
                            break;
                        case "Content-Type":
                            browserResponse.ContentType = value;
                            break;
                        case "Transfer-Encoding":
                            if (value == "chunked")
                                browserResponse.SendChunked = true;
                            else
                                browserResponse.ContentEncoding = Encoding.GetEncoding(value);
                            break;
                        default:
                            browserResponse.Headers.Add(key, value);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log(ServerLogItemType.Debug,
                        "setHeadersToBrowser: Error when header \"" + key + "\" copying\n" + ex);
                }
        }

        private void SetBrowserResponseData(HttpListenerResponse browserResponse, HttpWebResponse response)
        {
            var data = GetDataFromRemoteResponse(response);
            var modifiedData = ModifyBrowserResponseData(data, response);
            SetDataToBrowserResponse(browserResponse, modifiedData);
        }

        private byte[] GetDataFromRemoteResponse(HttpWebResponse response)
        {
            using var responseStream = response.GetResponseStream();
            var data = responseStream.ReadAllBytes();
            responseStream?.Close();
            return data;
        }

        protected virtual byte[] ModifyBrowserResponseData(byte[] data, HttpWebResponse response)
        {
            return data;
        }

        private void SetDataToBrowserResponse(HttpListenerResponse browserResponse, byte[] data)
        {
            browserResponse.ContentLength64 = data.Length;
            browserResponse.OutputStream.Write(data, 0, data.Length);
            browserResponse.OutputStream.Close();
        }
        #endregion

        public bool SetSecurityTokenAndCookieFromFirstPage()
        {
            var url = _baseUrl + "/";
            var request = ProxyServerBase.CreateGetRequest(url);
            request.FillCookiesFrom(Cookies);

            using var response = (HttpWebResponse)request.GetResponse();
            SecurityTokenValue = GetSecurityTokenValueOrNull(response.GetResponseText());
            Cookies.AddOrReplace(response.Cookies.AsEnumerable());
            if (string.IsNullOrWhiteSpace(SecurityTokenValue))
            {
                Log(ServerLogItemType.Abort, $"{url} returns no security token");
                return false;
            }

            Log(ServerLogItemType.Authorization,
                $"SetSecurityTokenAndCookieFromFirstPage Token = {SecurityTokenValue}");
            Log(ServerLogItemType.Authorization, $"SetSecurityTokenAndCookieFromFirstPage Cookies:\r\n {Cookies}");
            return true;

            string GetSecurityTokenValueOrNull(string firstPageText)
            {
                var securityTokenRegex = new Regex("name *= *\"__RequestVerificationToken\".*value=\"(?<token>.*)\"");
                var value = securityTokenRegex.Match(firstPageText).Groups["token"].Value;
                return value;
            }
        }
    }
}