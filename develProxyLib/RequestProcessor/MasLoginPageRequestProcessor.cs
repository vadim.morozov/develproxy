﻿using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace develProxy.lib
{
    public class MasLoginPageRequestProcessor : RequestProcessor
    {
        private readonly string _proxyUrl;
        private readonly Regex _targetUrlRegex = new Regex("\"targetUrl\" *: *\"(?<url>.*)/signmein\"");

        public MasLoginPageRequestProcessor(LogDelegate log, string baseRemoteUrl, string proxyUrl)
            : base(log, baseRemoteUrl)
        {
            _proxyUrl = proxyUrl;
        }

        public string PortalUrl { get; private set; }

        protected override byte[] ModifyBrowserResponseData(byte[] data, HttpWebResponse response)
        {
            string[] pathsForReplace = {"/api/loginIntoApplication", "/api/application"};

            if (!pathsForReplace.Contains(response.ResponseUri.AbsolutePath)) return data;

            return GetModified();

            byte[] GetModified()
            {
                var unzippedData = response.ContentEncoding == "gzip" ? GZip.Decompress(data) : data;
                var responseText = Encoding.UTF8.GetString(unzippedData);
                var matches = _targetUrlRegex.Match(responseText);
                if (!matches.Success) return data;

                PortalUrl = matches.Groups["url"].Value;

                var modifiedResponseText = responseText.Replace(PortalUrl, _proxyUrl);
                var modifiedUnzippedData = Encoding.UTF8.GetBytes(modifiedResponseText);
                var modifiedData = response.ContentEncoding == "gzip"
                    ? GZip.Compress(modifiedUnzippedData)
                    : modifiedUnzippedData;

                return modifiedData;
            }
        }
    }
}