﻿namespace develProxy.lib
{
    public class ProxySettings
    {
        public string ProxyPort { get; set; }
        public string ProxyHost { get; set; }
        public string LocalUrl { get; set; }
        public string RemoteUrl { get; set; }
        public string DataRequestRegexs { get; set; }
        public IModeSpecificSettings SpecificSettings { get; set; }
    }
}